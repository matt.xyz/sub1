import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio.component';
import { Version1Component } from './version1/version1.component';
import { Version2Component } from './version2/version2.component'


const routes: Routes = [
  { path: 'inicio', component: InicioComponent,
    children:
    [
      { path: 'version1', component: Version1Component },
      { path: 'version2', component: Version2Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InicioRoutingModule { }
