import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InicioRoutingModule } from './inicio-routing.module';
import { SharedModule } from '../shared/shared.module';
import { Version1Component } from './version1/version1.component';
import { Version2Component } from './version2/version2.component';
import { InicioComponent } from './inicio.component';


@NgModule({
  declarations: [
    InicioComponent,
    Version1Component,
    Version2Component
  ],
  imports: [
    CommonModule,
    InicioRoutingModule,
    SharedModule
  ]
})
export class InicioModule { }
